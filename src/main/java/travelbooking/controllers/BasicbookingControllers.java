package travelbooking.controllers;

import org.apache.coyote.Response;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import travelbooking.Model.Flights;
import travelbooking.Services.FlightSearchService;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

@RestController
public class BasicbookingControllers {
    @RequestMapping(value = "/hello")
    public String sayHello(){
        return "Hello Folks!Welcome to Travel Booking";
    }
   static Map<String, Flights> flights = new HashMap<>();

    /*@RequestMapping(value = "/availableFlights")
    public ResponseEntity<Object> availableFlights(){
        return new ResponseEntity<>(flights.values(), HttpStatus.OK);
    }*/

    @RequestMapping(value="/availableFlights")
    Collection<Flights> all(){
       return FlightSearchService.searchFlights();
    }



}
