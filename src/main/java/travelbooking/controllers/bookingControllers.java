package travelbooking.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class bookingControllers {
    @RequestMapping(value = "/hello")
    public String sayHello(){
        return "Hello Folks!Welcome to Travel Booking";
    }
}
