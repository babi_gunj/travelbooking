package travelbooking.Model;

public class Flights {
   public String flightName;
    public int noOfPassengers;
    public int noOfTotalSeats;  //"noOfPassengers"+"noOfSeatsLeft"
    public int noOfSeatsLeft;

    public int getNoOfSeatsLeft() {
        return noOfSeatsLeft;
    }

    public void setNoOfSeatsLeft(int noOfSeatsLeft) {
        this.noOfSeatsLeft = noOfSeatsLeft;
    }



    public int getNoOfTotalSeats() {
        return noOfTotalSeats;
    }

    public void setNoOfTotalSeats(int noOfTotalSeats) {
        this.noOfTotalSeats = noOfTotalSeats;
    }



    public int getNoOfPassengers() {
        return noOfPassengers;
    }

    public void setNoOfPassengers(int noOfPassengers) {
        this.noOfPassengers = noOfPassengers;
    }

    public String getFlightName() {
        return flightName;
    }

    public void setFlightName(String flightName) {
        this.flightName = flightName;
    }


    public Flights(String flightName,int noOfPassengers,int noOfSeatsLeft,int noOfTotalSeats){
        this.flightName = flightName;
        this.noOfPassengers = noOfPassengers;
        this.noOfSeatsLeft = noOfSeatsLeft;
        this.noOfTotalSeats = noOfTotalSeats;
    }



}
