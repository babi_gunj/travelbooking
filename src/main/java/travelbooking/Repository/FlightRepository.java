package travelbooking.Repository;

import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestMapping;
import travelbooking.Model.Flights;

import java.util.*;

@Repository
public class FlightRepository implements ObjectRepository<Flights> {

    public static Map<String, Flights> flightRepository;

    public FlightRepository(){
        this.flightRepository = new HashMap<>();

        Flights flight1 = new Flights("Boeing777", 0,100,100);
        Flights flight2 = new Flights("Airbus319", 0,100,100);
        Flights flight3 = new Flights("Airbus321", 0,100,100);
        System.out.println(flight1.flightName + flight1);
        flightRepository.put(flight1.flightName, flight1);
        flightRepository.put(flight2.flightName, flight2);
        flightRepository.put(flight3.flightName, flight3);
    }


    @Override
    public void store(Flights flight){
        flightRepository.put(flight.flightName,flight);
    }

    @Override
    public Flights retrieve() {
        return null;
    }

    @Override
    public Flights search() {
        return null;
    }

    @Override
    public Flights delete(int id) {
        return null;
    }



}
