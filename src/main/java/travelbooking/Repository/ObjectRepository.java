package travelbooking.Repository;


import travelbooking.Model.Flights;

import java.util.Collection;
import java.util.Map;

public interface ObjectRepository<T> {

        public void store(T t);

       public T retrieve();

       public T search();

       public T delete(int id);



}

